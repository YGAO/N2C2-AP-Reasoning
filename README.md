# 2022 N2C2 Track 3: Assessment and Plan Reasoning

This is the repository for sample data used in [2022 National NLP Clinical Challenge (N2C2)](https://n2c2.dbmi.hms.harvard.edu/2022-track-3), Track 3: Progress Note Understanding: Assessment and Plan Reasoning. The annotation on sample data is provided by [ICU Data Science Lab, Department of Medicine, SMPH, UW-Madison](https://www.medicine.wisc.edu/apcc/icu-data-science-lab). We conduct annotation on [MIMIC data](https://physionet.org/content/mimiciii-demo/1.4/), which requires data usage agreement. 

**Please refer to an LREC paper for the detailed annotation protocol and task description**: [Paper](https://arxiv.org/abs/2204.03035). 

### Table of Contents
**[Task](#task)**<br>
**[Example](#example)**<br>
**[Format](#format)**<br>
**[Data Usage](#data)**<br>
**[Authorship](#authorship)**<br>
**[Acknowledgement](#acknowledgement)**<br>
**[Contact](#contact)**<br>

### Task 

We will provide pairs of assessment section and plan subsection (A&P pair). The model should take A&P pairs as input and output one of the four relations: *Direct, Indirect, Neither, Not Relevant*.  The definitions of the four relations are the following: 
```
1) Direct, if the plan subsection targets the primary diagnoses or problem
2) Indirect, if the plan subsection lists problems/diagnoses (e.g. organ failure) associated with the primary diagnosis/problem, including other diagnoses/problems that are not part of the main diagnosis.   
3) Neither, if the plan subsection mentions a problem/diagnosis that is not mentioned in that day’s progress note. 
4) Not Relevant, if the plan subsection does not include a problem/diagnosis
```

![A&P Relation](AP_relation.png)
The center area is Assessment. There are 4 Plan Subsections and each of the Plan Subsection is labelled with a relation. 

### Example 

To explain the task better, we include two more example A&P pairs in the figure below: 

![Example A&P pair with the annotated relation](ap_samples.png)

In the first example, the plan subsection mentions "Respiratory failure", which is the main diagnose ("hospital-acquired pneumonia") included in the assessment, therefore the relation is "Direct".  

In the second example, the plan subsection mentions "Amenia". It is directly *caused* by the "upper GI bleed" mentioned in the assessment section. Therefore the relation between assessment and plan subsection is "Direct". 


### Format (Updated on April 14, 2022)

With the permission from PhysioNet, we provide raw text and character offsets for Assessment and Plan Subsection. The samples with raw text are in n2c2_sample_raw.csv file, with the following columns: ROW ID, HADM ID, Assessment, Plan Subsection, and Relation. If you are interested in extracting the assessment and plan subsection from MIMIC yourself, you could find an n2c2_sample.csv file with the following columns: ROW ID, HADM ID, Assessment Begin, Assessment End, PlanSubsection Begin, PlanSubsection End, Relation. You could match with the raw notes in MIMIC (NOTEEVENTS.csv) by matching the ROW ID and HADM ID. 

Every row is essentially a training/test sample. Given every pair of Assessment and Plan raw text as input (extracted through every row of records), N2C2 participants should develop model that predicts the Relation. The last column of the csv file provides the ground-truth labels indicating the relations between every pair of assessment and plan subsection (Direct, Indirect, Neither, Not Relevant). 


### Data Usage 
Participants must access the MIMIC data through this challenge or PhysioNet, and sign the MIMIC's data usage agreement. 

### Authorship
This project is a joint work from Dr. Yanjun Gao (UW-Madison ICU DataSci Lab), Samuel Tesch (UW-Madison DOM), Ryan Laffin (UW-Madison DOM), Dr. John Caskey (UW-Madison ICU DataSci Lab), Prof. Tim Miller (Boston Children's Hospital, Harvard), Prof. Dimitry Dligach (Loyola), Prof. Matthew Churpek (UW-Madison ICU DataSci Lab) and Prof. Majid Afshar (UW-Madison ICU DataSci Lab). 

### Acknowledgement
We thank Samuel Tesch and Ryan Laffin for their efforts in data annotation. We also thank Prof. Ozlem Uzuner (George Mason) and Harvard Medical School DBMI for hosting the challenge for us.  

### Contact
You could join our google [discussion group](https://groups.google.com/g/N2C2_UW_ICU_DataSci_2022_AP_Task). For other questions, please contact ygao@medicine.wisc.edu. 
